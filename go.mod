module gitlab.com/razzius/token-terminal

go 1.13

require (
	github.com/go-redis/redis/v7 v7.0.0-beta.5
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/motemen/go-quickfix v0.0.0-20200118031250-2a6e54e79a50 // indirect
	github.com/motemen/gore v0.5.0 // indirect
	golang.org/x/tools v0.0.0-20200119215504-eb0d8dd85bcc // indirect
)
