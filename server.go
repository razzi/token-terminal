package main

import (
	"fmt"
	"github.com/go-redis/redis/v7"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const USERS_HASH = "users"

const RIDE_PRICE = 3
const PASS_PRICE = 7
const TRANSFER_TIME = 5 * time.Second

func getUserKey(token string) string {
	userKey := strings.Join([]string{USERS_HASH, token}, ":")
	return userKey
}

func getTimeKey(tenSecondTime int) string {
	timeKey := strconv.Itoa(tenSecondTime)
	return timeKey
}

func getSpendForToken(client *redis.Client, tenSecondTime int, token string) (int, error) {
	// Redis stores a map of users:<token> to the 10-second times.
	userKey := getUserKey(token)
	timeKey := getTimeKey(tenSecondTime)

	currentCost, err := client.HGet(userKey, timeKey).Result()

	if err == redis.Nil {
		return 0, nil
	} else if err != nil {
		return 0, err
	} else {
		intCost, err := strconv.Atoi(currentCost)

		if err != nil {
			// This should never happen; it means garbage values are in redis
			client.HDel(userKey, timeKey)

			return 0, err
		}

		return intCost, nil
	}
}

func setCostForToken(client *redis.Client, tenSecondTime int, token string, cost int) {
	userKey := getUserKey(token)
	timeKey := getTimeKey(tenSecondTime)

	client.HSet(userKey, timeKey, cost)
}

func checkAndActivateTransfer(client *redis.Client, token string) (bool, error) {
	transferKey := fmt.Sprintf("transfer:%s", token)
	transferActive, err := client.Exists(transferKey).Result()

	if transferActive == 1 {
		return true, nil
	}

	if err != nil {
		return false, err
	}

	client.Set(transferKey, "ACTIVE", TRANSFER_TIME)

	return false, nil
}

func handler(w http.ResponseWriter, r *http.Request, client *redis.Client) {
	transactionTime := int(time.Now().Unix())

	tenSecondTime := (transactionTime / 60) * 60

	authorization := r.Header.Get("authorization")
	isToken := strings.HasPrefix(authorization, "token ")

	if !isToken {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("401 - Must specify authorization token"))
		return
	}

	token := strings.Join(strings.Split(authorization, " ")[1:], " ")

	userCost, err := getSpendForToken(client, tenSecondTime, token)

	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		log.Fatal(err)
		return
	}

	activeTransfer, err := checkAndActivateTransfer(client, token)

	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		log.Fatal(err)
		return
	}

	welcomeMessage := fmt.Sprintf("Welcome token %s, have a good ride", token)

	if activeTransfer {
		body := fmt.Sprintf(
			"%s. You have an active transfer. You have spent $%d this minute.",
			welcomeMessage, userCost,
		)
		w.Write([]byte(body))
		return
	}

	totalPrice := userCost + RIDE_PRICE

	var body string
	if totalPrice > PASS_PRICE {
		totalPrice = PASS_PRICE

		body = fmt.Sprintf("%s. You have a minute pass worth $%d.", welcomeMessage, PASS_PRICE)
	} else {
		// TODO not atomic
		body = fmt.Sprintf("%s. You've been charged $%d this minute.", welcomeMessage, totalPrice)
	}

	setCostForToken(client, tenSecondTime, token, totalPrice)

	w.Write([]byte(body))
}

func main() {
	fmt.Println("Connecting to redis...")
	client := redis.NewClient(&redis.Options{})

	fmt.Println("Checking redis ping...")
	pong, err := client.Ping().Result()

	if err != nil {
		panic(err)
		return
	}

	fmt.Println("Redis gave", pong)

	fmt.Println("Resetting redis state...")
	client.FlushAll()

	fmt.Println("HTTP serving on port 8080...")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handler(w, r, client)
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
